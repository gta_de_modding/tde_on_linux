# tde_on_linux

Preliminary guide for running GTA: TDE on Linux.

Guides will be written on a per-distro basis, so if you wish to backport a guide for another distribution - please feel free!

* [Arch Linux (and derivatives)](guides/arch_linux.MD)