# Arch Linux running guide

## Dependencies
* Wine (wine, wine-gecko, wine-mono)
* Winetricks
* DXVK

## Notes

* This guide is only applicable to the cracked copy. Where you source your game files is at __your own__ discretion, but we will assume you've acquired the files through official means.
We do not condone the usage of illicitly-acquired game copies.

* This guide only works with the day 0 patch of the game, as whiteee's crack does not function on Linux. You are advised to use ManiacKnight's crack.

* At the moment, running the game through an alternate wineprefix will display the "Couldn't start the game" error, which is very non-descriptive. Hence, you have to run the game under the default wineprefix. A solution to this will be looked into, nonetheless.

* D3D12 renderer support has not been looked into. D3D11 through DXVK works perfectly fine though.

## Guide

1. Install dependencies from pacman package manager.
```
sudo pacman -S wine winetricks wine-mono wine-gecko
```

2. Download the latest release of DXVK from [here](https://github.com/doitsujin/dxvk/releases).

3. Apply DXVK's d3d11.dll and dxgi.dll to your Gameface/Binaries/Win64 folder. ENSURE YOU COPY OVER X64 BINARIES!

4. Open Winetricks.

5. Click `Select the default wineprefix` in Winetricks.

6. Click `Run winecfg` in Winetricks.

7. Click on the `Libraries` tab, and apply `d3d11`, `dxgi` as DLL overrides.

8. Save your changes and exit.

9. Double click on Launcher.exe, and your game will run.